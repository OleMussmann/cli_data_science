# Data Science on the Command Line Interface (CLI)
Before deciding _how_ to accelerate certain programms/scripts, we first have to understand the nature of the problem.
Many small projects or exploratory work can be done with command line tools. If you have never worked on the Linux command line, or your skills are a bit rusty, feel free to have a look at a short [Linux CLI Tutorial](https://maker.pro/linux/tutorial/basic-linux-commands-for-beginners).

Now we will explore a dataset about the Academy Awards on the command line. Who won how many Oscars, and when?

## CLI Data Science at home
1. Download the data file.
```
wget isbjornlabs.com/BD_infra_2020/slides/03_databases/academy_awards.txt
```
2. Explore on the command line.
3. Feel free to come up with your own questions and answers!

## CLI Data Science in the cloud (for free)

### Warm up with Google Colab
0. Create a Google account at [google.com](https://google.com) if you don't have one yet
1. Open [Google Colab Notebooks](https://colab.research.google.com)
2. Click Examples
3. Click Overview of Colaboratory Features
4. Explore!
5. Feel free to check out more examples

### Exploration
0. If you need help with the command line, check out the [CLI cheatsheet](https://isbjornlabs.com/big_data_infra/slides/05_databases/cheatsheet_cli.html)
1. Download [CLI.ipynb](CLI.ipynb)
2. Open [Google Colab Notebooks](https://colab.research.google.com)
3. Click upload, upload `CLI.ipynb`
4. Run the cells and answer the questions below. We will be (ab)using the notebook to run Linux command line commands. Don't forget to include the `%%shell` header in the cells to make use of the underlying command line.
5. Feel free to come up with your own questions and answers!

## Answers
1. What does the header look like?
2. How long is the list?
3. When was Joaquin Phoenix born?
4. How many women are in the list?
5. Who has the most wins?
6. Which two actors have the second most nominations?